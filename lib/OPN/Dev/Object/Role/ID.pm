use utf8;
package OPN::Dev::Object::Role::ID;
our $VERSION = '0.001';
use Moose::Role;
use namespace::autoclean;
use UUID4::Tiny qw(create_uuid_string);

# ABSTRACT: An Object::Role::ID Role for OPN Development

has id => (
    is       => 'ro',
    isa      => 'Int',
    required => 1,
);

has uuid => (
    is      => 'ro',
    isa     => 'Str',
    builder => '_build_uuid',
);

sub _build_uuid {
    return create_uuid_string();
}

1;

__END__

=head1 DESCRIPTION

=head1 SYNOPSIS

    package Foo;
    with qw(OPN::Dev::Object::Role::ID);

    # by default you have
    # id
    # uuid

=head1 ATTRIBUTES

=head1 METHODS
