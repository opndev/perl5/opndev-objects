package OPN::Dev::Object::User;
use OPN::Dev::Object;

# ABSTRACT: A simple user object

has username => (
    isa      => 'Str',
    required => 1,
);

has password => (
    isa       => 'Str',
    predicate => 'has_password',
);


__PACKAGE__->meta->make_immutable;

__END__

=head1 DESCRIPTION

=head1 SYNOPSIS

    use OPN::Dev::Object::User;



