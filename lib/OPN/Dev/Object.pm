use utf8;

package OPN::Dev::Object;
our $VERSION = '0.001';

# ABSTRACT: A base class for objects

use Moose                ();
use namespace::autoclean ();

use Import::Into;
use Moose::Exporter;
use Moose::Util::MetaRole;

Moose::Exporter->setup_import_methods(
    with_meta => ['has'],
    also      => ['Moose'],
);


sub init_meta {
    my $class     = shift;
    my %params    = @_;
    my $for_class = $params{for_class};
    Moose->init_meta(@_);

    namespace::autoclean->import(-cleanee => $for_class);

    Moose::Util::MetaRole::apply_base_class_roles(
        for   => $for_class,
        roles => [
            qw(
                MooseX::Log::Log4perl
                OPN::Dev::Object::Role::ID
            )
        ]
    );

    return $for_class->meta();

}

sub has {
    my ($meta, $name, %options) = @_;

    $options{is}  //= 'ro';
    $options{isa} //= 'Defined';
    my $builder = $options{builder} && $options{builder} eq 1 ? 1 : 0;

    # "has [@attributes]" versus "has $attribute"
    foreach ('ARRAY' eq ref $name ? @$name : $name) {

        $meta->add_attribute(
            $_,
            %options,
            $builder ? (builder => "_build_${_}") : (),
            $options{builder} && !exists $options{lazy} ? (lazy => 1) : (),
        );
    }
}

1;

__END__

=head1 DESCRIPTION

=head1 SYNOPSIS

    package Foo;
    use OPN::Dev::Object;

    # by default you have
    # id
    # uuid

=head1 ATTRIBUTES

=head1 METHODS

=head2 init_meta

Override init_meta and automaticly applies the roles L<MooseX::Log::Log4perl>
and L<OPN::Dev::Object::Role::ID>

=head2 has

Default C<has> handler, automaticly sets C<isa> to C<Defined> and C<is> to
C<ro> if not defined by the user.

