use strict;
use warnings;
use Test::More 0.96;
use Test::Warnings;
use Test::Exception;

package TestMoose {
    use OPN::Dev::Object;

    has builder => (builder => 1);

    has builder_by_name => (builder => '_build_builder');

    has builder_lazy => (builder => '_builder_lazy');

    has _builder_lazy_attr => (
        is        => 'rw',
        predicate => 'has_lazy_builder'
    );

    sub _build_builder {
        'I have builder',;
    }

    sub _builder_lazy {
        my $self = shift;
        if ($self->has_lazy_builder) {
            return "has lazy builder";
        }
        die "Unable to build lazy";
    }


}


my $test = TestMoose->new(id => 1);
isa_ok($test, 'TestMoose');

is($test->builder,         'I have builder', 'builder => 1 works');
is($test->builder_by_name, 'I have builder', 'builder by name also works');

$test->_builder_lazy_attr(1);

is(
    $test->builder_lazy,
    "has lazy builder",
    "Lazy works when builder is defined"
);
done_testing;
