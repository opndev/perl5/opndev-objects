use strict;
use warnings;
use Test::More 0.96;
use Test::Warnings;

use OPN::Dev::Object::User;

my $test = OPN::Dev::Object::User->new(
    id       => 1,
    username => 'Foo',
);
isa_ok($test, 'OPN::Dev::Object::User');

isnt($test->uuid, undef, "We have a UUID");


done_testing;
