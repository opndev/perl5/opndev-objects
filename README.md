# DESCRIPTION

# SYNOPSIS

    package Foo;
    use OPN::Dev::Object;

    # by default you have
    # id
    # uuid

# ATTRIBUTES

# METHODS

## init\_meta

Override init\_meta and automaticly applies the roles [MooseX::Log::Log4perl](https://metacpan.org/pod/MooseX%3A%3ALog%3A%3ALog4perl)
and [OPN::Dev::Object::Role::ID](https://metacpan.org/pod/OPN%3A%3ADev%3A%3AObject%3A%3ARole%3A%3AID)

## has

Default `has` handler, automaticly sets `isa` to `Defined` and `is` to
`ro` if not defined by the user.
